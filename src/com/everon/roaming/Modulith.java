package com.everon.roaming;

import com.everon.roaming.hub.domain.RoamingService;
import com.everon.roaming.hub.ports.EVDataUpdatesAndRoutingPort;
import com.everon.roaming.hub.ports.OutboundProtocolPort;
import com.everon.roaming.hub.ports.Protocol;
import com.everon.roaming.protocol.ocpi.adapters.OCPIClient;
import com.everon.roaming.protocol.ocpi.adapters.OCPIEVDataUpdatesAndRouting;
import com.everon.roaming.protocol.ocpi.domain.OCPIService;

import java.util.Collections;
import java.util.Map;

public class Modulith {

    public static void main(String[] args) {
        // Hexagon "OCPI adapter": adapters
        var ocpiServiceEVDataUpdatesAndRoutingAdapter = new OCPIEVDataUpdatesAndRouting();


        // Hexagon "Hub": adapters
        var updatesAndRoutingAdapters = Collections.<EVDataUpdatesAndRoutingPort>singletonList(ocpiServiceEVDataUpdatesAndRoutingAdapter);
        var protocols = Map.<Protocol, OutboundProtocolPort>of(new Protocol() {}, new OCPIClient());

        // Business logic for all hexagons:

        RoamingService roamingService = new RoamingService(updatesAndRoutingAdapters, protocols);
        OCPIService ocpiService = new OCPIService(roamingService);
    }
}
