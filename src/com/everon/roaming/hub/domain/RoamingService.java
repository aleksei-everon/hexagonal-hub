package com.everon.roaming.hub.domain;

import com.everon.roaming.hub.ports.Protocol;
import com.everon.roaming.hub.ports.EVDataUpdatesAndRoutingPort;
import com.everon.roaming.hub.ports.OutboundProtocolPort;
import com.everon.roaming.hub.ports.RoamingProtocolPort;

import java.util.List;
import java.util.Map;

public class RoamingService implements RoamingProtocolPort {
    private List<EVDataUpdatesAndRoutingPort> perProtocolUpdatesAndRoutingPorts;
    private Map<Protocol, OutboundProtocolPort> protocols;

    public RoamingService(List<EVDataUpdatesAndRoutingPort> perProtocolUpdatesAndRoutingPorts,
                          Map<Protocol, OutboundProtocolPort> protocols) {
        this.perProtocolUpdatesAndRoutingPorts = perProtocolUpdatesAndRoutingPorts;
        this.protocols = protocols;
    }

    @Override
    public AuthorizationResult authorizeToken(String token) {
        Protocol protocol = lookUpProtocolByToken(token);
        OutboundProtocolPort protocolPort = protocols.get(protocol);
        OutboundProtocolPort.AuthorizationResult result = protocolPort.authorizeToken(token);
        return mapResult(result);
    }

    Protocol lookUpProtocolByToken(String token) {}

    RoamingProtocolPort.AuthorizationResult mapResult(OutboundProtocolPort.AuthorizationResult result) {
        switch (result) {
            case GRANTED:
                return AuthorizationResult.GRANTED;
            case DENIED:
                return AuthorizationResult.DENIED;
        }
    }
}
