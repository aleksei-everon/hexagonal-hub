package com.everon.roaming.hub.ports;

/**
 * Remember the topic with the same name on the TA diagram?
 */
public interface EVDataUpdatesAndRoutingPort {
    String protocolName();

    void sendEvDataUpdate(EVDataUpdateMessage message);

    void sendRoutingMessage(RoutingMessage message);

    interface EVDataUpdateMessage {}
    interface RoutingMessage {}
}
