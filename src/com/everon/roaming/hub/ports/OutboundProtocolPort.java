package com.everon.roaming.hub.ports;

public interface OutboundProtocolPort {

    AuthorizationResult authorizeToken(String token);

    enum AuthorizationResult {
        GRANTED,
        DENIED
    }
}
