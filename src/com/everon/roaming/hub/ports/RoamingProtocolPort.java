package com.everon.roaming.hub.ports;

public interface RoamingProtocolPort {

    AuthorizationResult authorizeToken(String token);

    enum AuthorizationResult {
        GRANTED,
        DENIED
    }
}
