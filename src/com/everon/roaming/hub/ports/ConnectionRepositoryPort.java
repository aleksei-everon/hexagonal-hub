package com.everon.roaming.hub.ports;

public interface ConnectionRepositoryPort {

    Protocol getProtocol(String targetPartyId);

}
