package com.everon.roaming.protocol.ocpi.domain;

import com.everon.roaming.hub.ports.RoamingProtocolPort;
import com.everon.roaming.protocol.ocpi.ports.OCPIProtocolPort;

public class OCPIService implements OCPIProtocolPort {
    private RoamingProtocolPort roamingProtocolPort;

    public OCPIService(RoamingProtocolPort roamingProtocolPort) {
        this.roamingProtocolPort = roamingProtocolPort;
    }

    @Override
    public AuthorizeTokenResponse authorizeToken(AuthorizeTokenRequest request) {
        var authorizationResult = roamingProtocolPort.authorizeToken(extractToken(request));

        return mapResponse(authorizationResult);
    }

    private static String extractToken(AuthorizeTokenRequest req) {
        return "whatever";
    }

    private static AuthorizeTokenResponse mapResponse(RoamingProtocolPort.AuthorizationResult res) {
        return null;
    }
}
