package com.everon.roaming.protocol.ocpi.adapters;

import com.everon.roaming.hub.ports.EVDataUpdatesAndRoutingPort;

public class OCPIEVDataUpdatesAndRouting implements EVDataUpdatesAndRoutingPort {
    @Override
    public String protocolName() {
        return "OCPI2.1.1";
    }

    @Override
    public void sendEvDataUpdate(EVDataUpdateMessage message) {

    }

    @Override
    public void sendRoutingMessage(RoutingMessage message) {

    }
}
