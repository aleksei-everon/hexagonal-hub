package com.everon.roaming.protocol.ocpi.adapters;

import com.everon.roaming.protocol.ocpi.ports.OCPIProtocolPort;

public class OCPIApi {

    private final OCPIProtocolPort ocpiProtocolPort;

    public OCPIApi(OCPIProtocolPort ocpiProtocolPort) {
        this.ocpiProtocolPort = ocpiProtocolPort;
    }

    // /authorize
    Resp authorize(Req req) {
        return mapResponse(ocpiProtocolPort.authorizeToken(mapRequest(req)));
    }

    private static OCPIProtocolPort.AuthorizeTokenRequest mapRequest(Req req) {}
    private static Resp mapResponse(OCPIProtocolPort.AuthorizeTokenResponse resp) {}

    interface Req {}
    interface Resp {}
}
