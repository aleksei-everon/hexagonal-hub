package com.everon.roaming.protocol.ocpi.ports;

public interface OCPIProtocolPort {

    AuthorizeTokenResponse authorizeToken(AuthorizeTokenRequest request);

    interface AuthorizeTokenRequest {
    }

    interface AuthorizeTokenResponse {
    }
}
