Disclaimer: this is not a "presentation", just my thinking I wrote down. 

## Goals of the MVP2

1. The modules of the monolith must be built around bounded contexts (domains)
2. Teams should be enabled (and encouraged) to develop, test and deploy the modules independently
3. Potential split into microservices (or submonoliths) should be kept in mind
4. Other concerns, like observability, operations, SLA/I/O, must also not cross the boundaries. 

## Principles

* "Component mindset": we are not working on a multimodule java project, but on several applications within one deployable.
* Structure enforcment 
  * Unidirectional dependencies
  * Explicit modularization guidelines, e.g hexagonal architectures
  * Testing for architectural rules (e.g. archunit)
  * NO to shared _modules_ ("commons", "DTOs", ), YES to shared libraries (reasonable amount)
  * Component tests must not cross the boundaries of the modules
* Anti-corruption layer
  * Including exceptions (monadic error handling FTW?)
  * Lots of mappings
  * Yes, it's fine to define an OpenAPI specification between components within the monolith

## Practices

* Hexagonal architecture is a good approach for modular monoliths
  * It's a "pattern" you can reason about in a conversation, when talking about modular monoliths
    * I.e: 
     — How exactly you modularize your monolith guys? 
     — Hexagonal architecture
     — Ah, got it
  * [DDD-friendly](https://vaadin.com/learn/tutorials/ddd/ddd_and_hexagonal)
  * Enforces the structure, and the dependency graph
  * Service-spilt friendly
  * Extremely awesome for component testing
* Gradle module per hexagon + one for the "driver"
